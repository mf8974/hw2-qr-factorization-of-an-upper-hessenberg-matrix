# Numerical mathematics hw2: QR factorization of an upper Hessenber matrix

In this homework we implemented QR factorization of an upper Hessenberg matrix and QR Iteration for finding eigenvalues and eigenvectors in such matrices. 

## How to build the program
Go into the /code directory and use your favorite C/C++ compiler as shown bellow.

* GCC
```bash
# build example
gcc example.cpp -o example
# produces example.exe on Win and example on Unix

# build tests
gcc tests.cpp -o tests
```

* clang
```bash
# build example
clang example.cpp -o example
# produces example.exe on Win and example on Unix

# build tests
clang tests.cpp -o tests
```
