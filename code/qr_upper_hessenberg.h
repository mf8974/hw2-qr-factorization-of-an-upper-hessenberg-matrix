#if !defined(QR_UPPER_HESSENBERG_H)
#define QR_UPPER_HESSENBERG_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>

#if DEBUG
#define Assert(Expression) if(!(Expression)) {*(int *)0 = 0;}
#else
#define Assert(Expression)
#endif

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef float f32;
typedef double f64;

// NOTE(miha): Structure for matrix with 'RowSize' rows and 'ColumnSize'
// columns. Elements are saved in one-dimensional array 'Elements' - need to be
// allocated!
struct matrix
{
    u32 RowSize;
    u32 ColumnSize;
    f64 *Elements;
};

// NOTE(miha): Structure for upper Hessenberg matrices. 'Length' is the size of
// the array 'Elements', while the 'Dimension' is the size of the upper
// Hessenber matrix (e.g. size of main diagonal).
//
// Elements are saved in one-dimensional array 'Elements', we are saving only
// the elements that can appear in the upper Hessenberg matrix i.e. the
// elements above the main diagonal and elements on the first diagonal bellow
// the main diagonal. We are saving the following Hessenberg matrix as shown in
// the array A:
//        1   2   3   4
//        5   6   7   8
//        0   9   10  11
//        0   0   12  13
//
// A[] = {1,2,3,4,5,6,7,8,9,10,11,12,13};
struct upper_hessenberg
{
    u32 Length;
    u32 Dimension;
    f64 *Elements;
};

// NOTE(miha): Structure for saving Givens rotation. We are eliminating the
// element in the 'J'th row with the element in the 'I'th row (in upper
// Hessenberg matrices 'I' is always one cell above 'J'). 'C' saves cosinus
// values of the angle and 'S' saves sinus value.
struct givens
{
    u32 I;
    u32 J;
    f64 C;
    f64 S;
};

// NOTE(miha): Structure for multiple Givens rotations. When we multiply
// individual Given rotations (e.g. Elements[Index]) together we obtain the
// orthogonal matrix Q.  'Length' is the length of the array 'Elements'.
struct givens_steps
{
    u32 Length;
    givens *Elements;
};

// NOTE(miha): Struct that has 'Q' and 'R' matrices obtained from QR
// factorization. We also save 'Steps' because they are useful for calculating
// QR iteration.
struct qr
{
    upper_hessenberg *R;
    matrix *Q;
    givens_steps *Steps;
};

// NOTE(miha): Struct for saving eigen values and eigen vectors. 'Length' is the
// number of eigen values and number of eigen vecotrs.
struct eigen
{
    u32 Length;
    f64 *Values;
    f64 *Vectors;
};

// NOTE(miha): Easy to understand (not fast) matrix multiplication of matrices
// 'A' and 'B'.
inline matrix
operator*(matrix A, matrix B)
{
    Assert(A.RowSize == B.ColumnSize);

    matrix TemporaryResult;
    TemporaryResult.RowSize = A.RowSize;
    TemporaryResult.ColumnSize = B.ColumnSize;
    TemporaryResult.Elements = (f64 *) malloc(sizeof(f64)*A.RowSize*B.ColumnSize);

    // NOTE(miha): Zero out memory given by malloc.
    for(u32 Index = 0; Index < A.RowSize*B.ColumnSize; ++Index)
    {
        TemporaryResult.Elements[Index] = 0.0f;
    }

    // NOTE(miha): Calculate multiplication of matrices 'A' and 'B'.
    for(u32 RowA= 0; RowA < A.RowSize; ++RowA)
    {
        for(u32 ColumnB = 0; ColumnB < B.ColumnSize; ++ColumnB)
        {
            for(u32 Index = 0; Index < B.ColumnSize; ++Index)
            {
                TemporaryResult.Elements[ColumnB + RowA*B.ColumnSize] += 
                    A.Elements[Index + RowA*A.RowSize] * B.Elements[ColumnB + Index*B.RowSize];
            }
        }
    }

    for(u32 Index = 0; Index < A.RowSize*A.ColumnSize; ++Index)
    {
        A.Elements[Index] = TemporaryResult.Elements[Index];
    }

    free(TemporaryResult.Elements);

    return A;
}

// NOTE(miha): Create Givens matrix with dimension 'Dimension' at row&column
// 'I' and 'J'.  Cosinus of the angles is in 'C' and sinus is in 'S'.
//
// Example:
//           I J
//   1 0 ... 0 0 ... 0 0
//   0 1     0 0     0 0
// I 0 0     C S     0 0
// J 0 0    -S C     0 0
//   0 0     0 0     1 0
//   0 0 ... 0 0 ... 0 1
//
// NOTE(miha): Just a helper function, in the QR algorithms we are not doing
// matrix by matrix multiplications...
inline matrix
CreateGivensMatrix(u32 I, u32 J, u32 Dimension, f64 C, f64 S)
{
    Assert(I != J);

    matrix Result;
    Result.RowSize = Dimension;
    Result.ColumnSize = Dimension;
    Result.Elements = (f64 *) malloc(sizeof(f64)*Dimension*Dimension);

    for(u32 Column = 0; Column < Dimension; ++Column)
    {
        for(u32 Row = 0; Row < Dimension; ++Row)
        {
            if(Row == Column)
                Result.Elements[Row + Column*Dimension] = 1.0f;
            else
                Result.Elements[Row + Column*Dimension] = 0.0f;
        }
    }

    Result.Elements[I + I*Dimension] = C;
    Result.Elements[I + J*Dimension] = S;
    Result.Elements[J + I*Dimension] = -S;
    Result.Elements[J + J*Dimension] = C;

    return Result;
}

// NOTE(miha): Helper function for printing matrices of type 'matrix'.
inline void
PrintMatrix(matrix *M)
{
    for(u32 Column = 0; Column < M->ColumnSize; ++Column)
    {
        for(u32 Row = 0; Row < M->RowSize; ++Row)
        {
            printf("%f, ", M->Elements[Row + Column*M->RowSize]);
        }
        printf("\n");
    }
}

// NOTE(miha): Helper function for printing one Givens matrix.
inline void
PrintGivensMatrix(givens *G, u32 Dimension)
{
    matrix Result = CreateGivensMatrix(G->I, G->J, Dimension, G->C, G->S);
    PrintMatrix(&Result);
    free(Result.Elements);
}

// NOTE(miha): Helper function for printing all the Givens steps in the array
// 'Steps'.
inline void
PrintGivensSteps(givens_steps *Steps, u32 Dimension)
{
    for(u32 Index = 0; Index < Steps->Length; ++Index)
    {
        printf("Givens matrix (step) %d\n", Index);
        PrintGivensMatrix(&Steps->Elements[Index], Dimension);
        printf("------------\n");
    }
}

// NOTE(miha): Helper function that transformms Hessenberg matrix 'H' to normal
// matrix.
inline matrix
HessenbergToMatrix(upper_hessenberg *H)
{
    matrix Result = {};
    Result.RowSize = H->Dimension;
    Result.ColumnSize = H->Dimension;
    Result.Elements = (f64 *) malloc(sizeof(f64) * Result.RowSize *
                                     Result.ColumnSize);
    // NOTE(miha) 'Shift' tells us how many indexes we must shift back to get
    // the right element - because we are only saving non-zero elements in
    // Hessenberg matrix.
    u32 Shift = 0;

    for(i32 Column = 0; Column < (i32)H->Dimension; ++Column)
    {
        if(Column >= 2)
        {
            Shift += Column - 1;
        }

        for(i32 Row = 0; Row < (i32)H->Dimension; ++Row)
        {
            if(Row <= Column-2)
                Result.Elements[Row + Column*Result.RowSize] = 0.0f;
            else
                Result.Elements[Row + Column*Result.RowSize] = H->Elements[Row + Column*H->Dimension - Shift];
        }
    }

    return Result;
}

// NOTE(miha): Helper function for printing the upper Hessenberg matrix 'H'.
inline void
PrintHessenbergMatrix(upper_hessenberg *H)
{
    // NOTE(miha) 'Shift' tells us how many indexes we must shift back to get
    // the right element - because we are only saving non-zero elements in
    // Hessenberg matrix.
    u32 Shift = 0;

    for(i32 Column = 0; Column < (i32)H->Dimension; ++Column)
    {
        if(Column >= 2)
        {
            Shift += Column - 1;
        }

        for(i32 Row = 0; Row < (i32)H->Dimension; ++Row)
        {
            if(Row <= Column-2)
                printf("%10.6f, ", 0.0f);
            else
                printf("%10.6f, ", H->Elements[Row + Column*H->Dimension - Shift]);
        }
        printf("\n");
    }
}

// NOTE(miha): Helper function for copying upper Hessenber matrix 'Source' to
// the upper Hessenberg matrix 'Destination'. 
// CARE(miha): We assume that both 'Source' and 'Destination' are already
// initialized with 'Length', 'Dimension' and have allocated heap memory for
// the 'Elements'.
inline void
CopyUpperHessenberMatrix(upper_hessenberg *Source, upper_hessenberg *Destination)
{
    Assert(Source->Length == Destination->Length);
    Assert(Source->Dimension == Destination->Dimension);
    Assert(Source->Elements != NULL);
    Assert(Destination->Elements != NULL);

    for(u32 Index = 0; Index < Source->Length; ++Index)
    {
        Destination->Elements[Index] = Source->Elements[Index];
    }
}

// NOTE(miha): Function for multiplying Givens matrix 'G' with an upper
// Hessenberg matrix 'H'. Final result is saved in the matrix 'H'. Here we are
// not multiplying whole matrices, but just the row&column the Givens matrix
// affects.
inline void
MultiplyGivensWithUpperHessenberg(givens *G, upper_hessenberg *H)
{
    upper_hessenberg TemporaryResult = {};
    TemporaryResult.Length = H->Length;
    TemporaryResult.Dimension = H->Dimension;
    TemporaryResult.Elements = (f64 *) malloc(sizeof(f64) * TemporaryResult.Length);

    CopyUpperHessenberMatrix(H, &TemporaryResult);

    // NOTE(miha): Triangle number of j (one row bellow i) is incremented by 1
    // e.g. TN(j) = TN(i) + 1. Triangle number for I&J represents how many
    // elements we "skipped" (because we are not saving elements bellow
    // subdiagonal for upper Hessenberg matrices).
    u32 TriangleNumberI = (G->I < 2) ? 0 : ((G->I-1)*G->I)/2;
    u32 TriangleNumberJ = (G->J < 2) ? 0 : TriangleNumberI+1;

    for(u32 Index = 0; Index < H->Dimension - G->I; ++Index)
    {
        f64 Upper = TemporaryResult.Elements[Index+G->I + H->Dimension*G->I - TriangleNumberI];
        f64 Lower = 0.0f;

        if(G->I == 0)
            Lower = TemporaryResult.Elements[Index + (G->J*H->Dimension) - TriangleNumberJ];
        else
            Lower = TemporaryResult.Elements[Index + (G->J*H->Dimension) - TriangleNumberJ + 1];

        f64 Mult = G->C*Upper + (-G->S*Lower);
        H->Elements[Index+G->I + H->Dimension*G->I - TriangleNumberI] = Mult;
    }

    for(u32 Index = 0; Index < H->Dimension - G->I; ++Index)
    {
        f64 Upper = TemporaryResult.Elements[Index+G->I + H->Dimension*G->I - TriangleNumberI];
        f64 Lower = 0.0f;

        if(G->I == 0)
            Lower = TemporaryResult.Elements[Index + (G->J*H->Dimension) - TriangleNumberJ];
        else
            Lower = TemporaryResult.Elements[Index + (G->J*H->Dimension) - TriangleNumberJ + 1];

        f64 Mult = G->S*Upper + G->C*Lower;

        if(G->I == 0)
            H->Elements[Index + (G->J*H->Dimension) - TriangleNumberJ] = Mult;
        else
            H->Elements[Index + (G->J*H->Dimension) - TriangleNumberJ + 1] = Mult;
    }

    free(TemporaryResult.Elements);
}

// NOTE(miha): Function for multiplying an upper Hessenberg matrix 'H' with
// Givens matrix 'G'. Final result is saved in the matrix 'H'. Here we are not
// multiplying whole matrices, but just the row&column the Givens matrix
// affects.
inline void
MultiplyUpperHessenbergWithGivens(upper_hessenberg *H, givens *G)
{
    upper_hessenberg TemporaryResult = {};
    TemporaryResult.Length = H->Length;
    TemporaryResult.Dimension = H->Dimension;
    TemporaryResult.Elements = (f64 *) malloc(sizeof(f64) * TemporaryResult.Length);

    CopyUpperHessenberMatrix(H, &TemporaryResult);

    for(u32 Index = 0; Index < G->I + 2; ++Index)
    {
        u32 TriangleNumberI = (Index < 2) ? 0 : ((Index-1)*Index)/2;
        u32 LeftIndex = G->I + Index*H->Dimension-TriangleNumberI;
        f64 Left = TemporaryResult.Elements[LeftIndex];
        f64 Right = TemporaryResult.Elements[LeftIndex+1];

        f64 MultFirstColumn = G->C*Left + -(G->S)*Right;
        H->Elements[LeftIndex] = MultFirstColumn;

        f64 MultSecondColumn = G->S*Left + G->C*Right;
        H->Elements[LeftIndex+1] = MultSecondColumn;
    }

#if 0
    // NOTE(miha): Second column.
    for(u32 Index = 0; Index < G->I + 2; ++Index)
    {
        u32 TriangleNumberI = (Index < 2) ? 0 : ((Index-1)*Index)/2;
        u32 LeftIndex = G->I + Index*H->Dimension-TriangleNumberI;
        f64 Left = TemporaryResult.Elements[LeftIndex];
        f64 Right = TemporaryResult.Elements[LeftIndex+1];

        f64 Mult = G->S*Left + G->C*Right;

        H->Elements[LeftIndex+1] = Mult;
    }
#endif

    free(TemporaryResult.Elements);
}

#if 0
// TODO(miha): Don't use operator* but just create a function thaht changes the
// elements in the original giveen matrix H.
inline upper_hessenberg
operator*(givens G, upper_hessenberg H)
{
    upper_hessenberg Result = {};
    Result.Length = H.Length;
    Result.Dimension = H.Dimension;
    Result.Elements = (f64 *) malloc(sizeof(f64)*Result.Length);

    CopyUpperHessenberMatrix(&H, &Result);

    // NOTE(miha): Triangle number of j (one row bellow i) is incremented by 1
    // e.g. TN(j) = TN(i) + 1. Here we calculate only triangle number for i. 
    u32 TriangleNumberI = (G.I < 2) ? 0 : ((G.I-1)*G.I)/2;
    u32 TriangleNumberJ = (G.J < 2) ? 0 : TriangleNumberI+1;

    for(u32 Index = 0; Index < H.Dimension - G.I; ++Index)
    {
        f64 Upper = H.Elements[Index+G.I + H.Dimension*G.I - TriangleNumberI];
        f64 Lower = 0.0f;

        if(G.I == 0)
            Lower = H.Elements[Index + (G.J*H.Dimension) - TriangleNumberJ];
        else
            Lower = H.Elements[Index + (G.J*H.Dimension) - TriangleNumberJ + 1];

        f64 Mult = G.C*Upper + (-G.S*Lower);
        Result.Elements[Index+G.I + H.Dimension*G.I - TriangleNumberI] = Mult;
    }

    for(u32 Index = 0; Index < H.Dimension - G.I; ++Index)
    {
        f64 Upper = H.Elements[Index+G.I + H.Dimension*G.I - TriangleNumberI];
        f64 Lower = 0.0f;

        if(G.I == 0)
            Lower = H.Elements[Index + (G.J*H.Dimension) - TriangleNumberJ];
        else
            Lower = H.Elements[Index + (G.J*H.Dimension) - TriangleNumberJ + 1];

        f64 Mult = G.S*Upper + G.C*Lower;

        if(G.I == 0)
            Result.Elements[Index + (G.J*H.Dimension) - TriangleNumberJ] = Mult;
        else
            Result.Elements[Index + (G.J*H.Dimension) - TriangleNumberJ + 1] = Mult;
    }

    return Result;
}
#endif

// NOTE(miha): Function creates identiy matrix in square matrix 'M'.
inline void
CreateIdentity(matrix *M)
{
    Assert(M->RowSize == M->ColumnSize);

    for(u32 Column = 0; Column < M->ColumnSize; ++Column)
    {
        for(u32 Row = 0; Row < M->RowSize; ++Row)
        {
            if(Row == Column)
                M->Elements[Row + Column*M->RowSize] = 1.0f;
            else
                M->Elements[Row + Column*M->RowSize] = 0.0f;
        }
    }
}

#if 0
// NOTE(miha): Return transpose of the matrix 'A'.
// CARE(miha): We are allocating memory on the heap (e.g. malloc) for
// 'Elements', so remember to free the memory when done.
inline matrix
TransposeMatrix(matrix *A) 
{
    matrix Result = {};
    Result.RowSize = A->RowSize;
    Result.ColumnSize = A->ColumnSize;
    Result.Elements = (f64 *) malloc(sizeof(f64)*Result.RowSize*Result.ColumnSize);

    for(u32 Column = 0; Column < Result.ColumnSize; ++Column)
    {
        for(u32 Row = 0; Row < Result.RowSize; ++Row)
        {
            Result.Elements[Column + Row*Result.RowSize] = A->Elements[Row + Column*A->RowSize];
        }
    }
    return Result;
}
#endif

// NOTE(miha): Here we are initing default values for the 'qr' struct.
// CARE(miha): We are allocating memory on the heap (e.g. malloc) for
// 'Elements', so remember to free the memory when done.
inline qr
InitQRStruct(upper_hessenberg *H)
{
    qr Result = {};

    // NOTE(miha): Allocate memory for upper Hessenberg matrix 'R'.
    Result.R = (upper_hessenberg *) malloc(sizeof(upper_hessenberg));
    Result.R->Length = H->Length;
    Result.R->Dimension = H->Dimension;
    Result.R->Elements = (f64 *) malloc(sizeof(f64) * Result.R->Length);

    CopyUpperHessenberMatrix(H, Result.R);

    // NOTE(miha): Allocate memory for orthogonal matrix 'Q'.
    Result.Q = (matrix *) malloc(sizeof(matrix));
    Result.Q->RowSize = Result.R->Dimension;
    Result.Q->ColumnSize = Result.R->Dimension;
    Result.Q->Elements = (f64 *) malloc(sizeof(f64) * Result.Q->RowSize *
                                        Result.Q->ColumnSize);

    CreateIdentity(Result.Q);

    // NOTE(miha): Allocate memory for sequence of Givens matrices 'Steps'.
    Result.Steps = (givens_steps *) malloc(sizeof(givens_steps));
    Result.Steps->Length = Result.R->Dimension - 1;
    Result.Steps->Elements = (givens *) malloc(sizeof(givens) * Result.Steps->Length);

    return Result;
}

// NOTE(miha): Function that free the memory of a 'qr' struct.
inline void
FreeQRStruct(qr *QR)
{
    free(QR->R->Elements);
    free(QR->Q->Elements);
    free(QR->Steps->Elements);
}

// NOTE(miha): Copies matrix 'Source' to thhe matrix 'Destination'.
// TODO(miha): Here we are allocating memory for destination... interesting...
// I think we just accidenttily double allocatted memory...
inline void
CopyMatrix(matrix *Source, matrix *Destination)
{
    Assert(Source->RowSize == Destination->RowSize);
    Assert(Source->ColumnSize == Destination->ColumnSize);
    // Destination->RowSize = Source->RowSize;
    // Destination->ColumnSize = Source->ColumnSize;
    // Destination->Elements = (f64 *) malloc(sizeof(f64) * Source->RowSize *
    //                                        Source->ColumnSize);

    for(u32 Index = 0; Index < Source->RowSize*Source->ColumnSize; ++Index)
    {
        Destination->Elements[Index] = Source->Elements[Index];
    }
}

// NOTE(miha): Multiplying matrix 'M' with Givens matrix 'G'. We need this for
// computing the final orhthogonal matrix 'Q' from QR factorization. We are
// only multuplying the rows&colums the 'G' affects.
inline void
MultiplyMatrixWithGivens(matrix *M, givens *G)
{
    u32 MatrixRowSize = M->RowSize;
    u32 MatrixColumnSize = M->ColumnSize;

    Assert(MatrixRowSize == MatrixColumnSize);
    Assert(G->I < MatrixRowSize && G->I < MatrixRowSize);
    Assert(G->J < MatrixRowSize && G->J < MatrixRowSize);

    matrix TemporaryResult = {};
    TemporaryResult.RowSize = MatrixRowSize;
    TemporaryResult.ColumnSize = MatrixColumnSize;
    TemporaryResult.Elements = (f64 *) malloc(sizeof(f64) * MatrixRowSize * MatrixColumnSize);

    CopyMatrix(M, &TemporaryResult);

    for(u32 Index = 0; Index < G->I + 2; ++Index)
    {
        f64 Left = TemporaryResult.Elements[G->I + Index*MatrixRowSize];
        f64 Right = TemporaryResult.Elements[G->J + Index*MatrixRowSize];

        f64 MultFirstColumn = G->C*Left + -(G->S)*Right;
        M->Elements[G->I + Index*MatrixRowSize] = MultFirstColumn;

        f64 MultSecondColumn = G->S*Left + G->C*Right;
        M->Elements[G->J + Index*MatrixRowSize] = MultSecondColumn;
    }

#if 0
    // NOTE(miha): Second column.
    for(u32 Index = 0; Index < G->I + 2; ++Index)
    {
        f64 Left = TemporaryResult.Elements[G->I + Index*MatrixRowSize];
        f64 Right = TemporaryResult.Elements[G->J + Index*MatrixRowSize];

        f64 Mult = G->S*Left + G->C*Right;
        M->Elements[G->J + Index*MatrixRowSize] = Mult;
    }
#endif

    free(TemporaryResult.Elements);
}

// NOTE(miha): QR factorization of an upper Hessenber matrix 'H'. We first
// eliminate elements on the lower subdiagonal with Givens rotation (matrix).
// This is how ww obtain upper triangular matrix 'R'. For the orthogonal matrix
// 'Q' we multiply Givens rotations together into one matrix.
inline qr
QRFactorization(upper_hessenberg *H)
{
    qr Result = InitQRStruct(H);

    // NOTE(miha): 'NextHopX' and 'NextHopY' keeps track from which location in
    // the array we take values - indxes into array. X is a value on the main
    // diagonal and Y is the value one cell bellow X, with Givens rotation we
    // eliminate Y and we obtain upper triagonal matrix R.
    u32 NextHopX = 0;
    u32 NextHopY = Result.R->Dimension;

    for(u32 Index = 0; Index < (Result.R->Dimension - 1); ++Index)
    {
        f64 X = Result.R->Elements[NextHopX];
        f64 Y = Result.R->Elements[NextHopY];
        f64 R = sqrt(X*X + Y*Y);
        f64 C = X/R;
        f64 S = -Y/R;

        givens G = {Index, Index+1, C, S};
        Result.Steps->Elements[Index] = G;

        // NOTE(miha): Calculate resulting 'R' matrix.
        MultiplyGivensWithUpperHessenberg(&G, Result.R);

        // NOTE(miha): Calculate resulting 'Q' matrix.
        MultiplyMatrixWithGivens(Result.Q, &G);

        NextHopX += H->Dimension-Index+1;
        NextHopY += H->Dimension-Index;
    }

    return Result;
}

// NOTE(miha): Extract eigen values from the given matrix 'H'.
// CARE(miha): QRIteration must first be applied on the matrix 'H'.
// CARE(miha): Caller must free allocated memory!
inline eigen
ExtractEigenValues(upper_hessenberg *H)
{
    eigen Result = {};

    // NOTE(miha): Retrieve eigenvalues from the main diagonal.
    f64 *EigenValues = (f64 *) malloc(sizeof(f64) * H->Dimension);
    u32 Shift = 0;

    Result.Length = H->Dimension;
    Result.Values = EigenValues;
    Result.Vectors = NULL;

    u32 NextHop = 0;
    for(u32 Index = 0; Index < H->Dimension; ++Index)
    {
        EigenValues[Index] = H->Elements[NextHop];
        NextHop += H->Dimension-Index+1;
    }

    return Result;
}

// NOTE(miha): Extract eigen vectors from the matrix 'H'. We also need eigen
// values, that are passed in 'Eigen'. 
// CARE(miha): The matrix 'H' must be "original", e.g. must not be the
// resulting matrix from the QRIteration.
inline eigen
ExtractEigenVectors(upper_hessenberg *H, eigen *Eigen)
{
    eigen Result = {};
    f64 *EigenValues = Eigen->Values;
    Result.Length = Eigen->Length;
    Result.Values = Eigen->Values;
    Result.Vectors = (f64 *) malloc(sizeof(f64) * Result.Length * Result.Length);

    // NOTE(miha): For each eigen value we solve the linear sytem with Gaussian
    // elimination.
    for(u32 EigenValueIndex = 0; EigenValueIndex < H->Dimension; ++EigenValueIndex)
    {
        f64 EigenValue = EigenValues[EigenValueIndex];

        upper_hessenberg A = {};
        A.Dimension = H->Dimension;
        A.Length = H->Length;
        A.Elements = (f64 *) malloc(sizeof(f64) * A.Length);
        CopyUpperHessenberMatrix(H, &A);

        // NOTE(miha): Subtract eigen value from the diagonal of the matrix
        // 'H', result is saved in the temporary matrix 'A'.
        u32 NextHop = 0;
        for(u32 Index = 0; Index < A.Dimension; ++Index)
        {
            A.Elements[NextHop] -= EigenValue;
            NextHop += A.Dimension-Index+1;
        }

        // NOTE(miha): Forward subtitution.
        u32 NextHopX = 0;
        u32 NextHopY = A.Dimension;
        for(u32 Index = 0; Index < A.Dimension-1; ++Index)
        {
            f64 Mult = A.Elements[NextHopY]/A.Elements[NextHopX];

            for(u32 Row = 0; Row < (A.Dimension - Index); ++Row)
            {
                f64 X = A.Elements[NextHopX+Row] * Mult;
                A.Elements[NextHopY+Row] -= X;
            }

            NextHopX += A.Dimension-Index+1;
            NextHopY += A.Dimension-Index;
        }

        // NOTE(miha): Set right side of the equation to 0.
        f64 *RightSides = (f64 *) malloc(sizeof(f64) * A.Dimension);
        for(u32 Index = 0; Index < A.Dimension; ++Index)
        {
            RightSides[Index] = 0.0f;
        }

        RightSides[A.Dimension-1] = 1.0f;
        // NOTE(miha): We calculate values from bottom up, we skip first row.
        u32 PreviousLength = A.Length-2;
        // NOTE(miha): Backward subtitution.
        for(u32 Index = 1; Index < A.Dimension; ++Index)
        {
            u32 TriangleNumber = ((A.Dimension+1-Index)*A.Dimension-Index)/2;
            f64 RowSum = 0.0f;
            PreviousLength--;
            for(u32 Row = 0; Row < Index; ++Row)
            {
                RowSum += -A.Elements[PreviousLength]*RightSides[A.Dimension-1-Row];
                PreviousLength--;
            }

            f64 Divider = A.Elements[PreviousLength--];
            RightSides[A.Dimension-1-Index] = RowSum/Divider;
        }

        // NOTE(miha): Copy right side vector values into result.
        for(u32 Index = 0; Index < A.Dimension; ++Index)
        {
            Result.Vectors[Index + EigenValueIndex*A.Dimension] = RightSides[Index];
        }

        free(RightSides);
    }

    return Result;
}

// NOTE(miha): Function for extracting eigen values and eigen vectors from
// matrix 'H'. Matrix 'ResultFromQRIteration' is the resulting matrix from QR
// iteration.
// CARE(miha): Caller must free
inline eigen
ExtractEigenValuesAndVectors(upper_hessenberg *ResultFromQRIteration, upper_hessenberg *H)
{
    eigen Values = ExtractEigenValues(ResultFromQRIteration);
    eigen ValuesAndVectors = ExtractEigenVectors(H, &Values);
    // free(Values.Values);
    return ValuesAndVectors;
}

// NOTE(miha): Helper function for printing eigen values.
inline void
PrintEigenValues(eigen *Eigen)
{
    for(u32 Index = 0; Index < Eigen->Length; ++Index)
    {
        printf("EigenValue %d: %f\n", Index, Eigen->Values[Index]);
    }
}

// NOTE(miha): Helper function for printing eigen vecotrs.
inline void
PrintEigenVectors(eigen *Eigen)
{
    for(u32 Index = 0; Index < Eigen->Length; ++Index)
    {
        printf("EigenVector %d: ", Index);

        for(u32 Row = 0; Row < Eigen->Length; ++Row)
        {
            printf("%f, ", Eigen->Vectors[Row + Index * Eigen->Length]);
        }
        printf("\n");
    }
}

// NOTE(miha): Algorithm for transforming upper Hessenber matrix 'H' into upper 
// triangular matrix. After enough iterations eigen values appears on the
// diagonal. 'NumberOfIterations' is the number of iteration QRIteration will
// do. We don't do anything fancy as shifting to achieve better convergence.
inline upper_hessenberg
QRIteration(upper_hessenberg *H, u32 NumberOfIterations)
{
    upper_hessenberg Result = {};
    Result.Dimension = H->Dimension;
    Result.Length = H->Length;
    Result.Elements = (f64 *) malloc(sizeof(f64) * Result.Length);
    CopyUpperHessenberMatrix(H, &Result);

    for(u32 Iteration = 0; Iteration < NumberOfIterations; ++Iteration)
    {
        qr QR = QRFactorization(&Result);

        // NOTE(miha): Basically: A = R*Q
        for(u32 Index = 0; Index < QR.Steps->Length; ++Index)
        {
            givens G = QR.Steps->Elements[Index];
            G.S = (G.S);
            MultiplyUpperHessenbergWithGivens(QR.R, &G);
        }

        CopyUpperHessenberMatrix(QR.R, &Result);
    }

    return Result;
}

// NOTE(miha): Function for calculating eigen values and vectors from upper
// Hessenberg matrix 'H'. Function calls QRIteration to find eigen values and 
// uses Gaussian elimination to find eigen vectors.
inline eigen
Eigen(upper_hessenberg *H)
{
    upper_hessenberg QRIterationH = QRIteration(H, 100);
    eigen Result = ExtractEigenValuesAndVectors(&QRIterationH, H);

    return Result;
}

// NOTE(miha): Frees eigen 'E' heap allocated memory.
inline void
FreeEigen(eigen *E)
{
    free(E->Values);
    free(E->Vectors);
}

// NOTE(miha): Creates an upper Hessenber matrix from the array 'Array' that
// has length 'Length'.
inline upper_hessenberg
CreateUpperHessenbergMatrix(f64 *Array, u32 Length)
{
    u32 Dimension = (Length == 4) ? 2 : ((u32)sqrt(Length))+1;

    upper_hessenberg Result = {Length, Dimension, Array};
    return Result;
}

#endif // QR_UPPER_HESSENBERG_H

#if 0
int
main()
{
    f64 Array[] = {0.0f,  1.0f,  2.0f,  3.0f,
                   4.0f,  5.0f,  6.0f,  7.0f, 
                          8.0f,  9.0f,  10.0f,
                                 11.0f, 12.0f};

    f64 M1Array[] = {0.0f, 1.0f, 2.0f,
                     3.0f, 4.0f, 5.0f,
                     6.0f, 7.0f, 8.0f};

    f64 I3Array[] = {1.0f, 0.0f, 0.0f,
                     0.0f, 1.0f, 0.0f,
                     0.0f, 0.0f, 1.0f};

    f64 I4Array[] = {1.0f, 0.0f, 0.0f, 0.0f,
                     0.0f, 1.0f, 0.0f, 0.0f,
                     0.0f, 0.0f, 1.0f, 0.0f,
                     0.0f, 0.0f, 0.0f, 1.0f};

    f64 H2[] = {0.0f, 1.0f, 
                2.0f, 3.0f};

    f64 H3[] = {0.0f, 1.0f, 2.0f,
                3.0f, 4.0f, 5.0f,
                      6.0f, 7.0f};

    f64 H31[] = { 1.0000,  -1.8084,   1.3152,
                 -6.0828,  -0.6757,  -0.0541,
                            1.9459,  -1.3243};

    f64 H4[] = {0.0f,  1.0f,  2.0f,  3.0f,
                4.0f,  5.0f,  6.0f,  7.0f, 
                       8.0f,  9.0f,  10.0f,
                              11.0f, 12.0f};

    f64 H4E[] = {5.0f,  1.0f,  1.0f,  1.0f,
                 1.0f,  5.0f,  1.0f,  1.0f, 
                        1.0f,  5.0f,  1.0f,
                               1.0f,  5.0f};

    f64 H5[] = {0.0f,  1.0f,  2.0f,  3.0f,  4.0f,  
                5.0f,  6.0f,  7.0f,  8.0f,  9.0f,
                       10.0f, 11.0f, 12.0f, 13.0f,
                              14.0f, 15.0f, 16.0f,
                                     17.0f, 18.0f};

    f64 H51[] = { 1.0000,   -5.0200,    1.2494,   -0.8707,   -2.7351,
                 -8.3666,   16.4143,   -9.2949,    0.7812,    8.0734,
                            -7.7108,    5.9522,    1.8707,   -3.1305,
                                        4.7699,    2.9264,   -4.1657,
                                                  -2.2964,   -2.2929};

    f64 H5Y[] = {2.0f, 3.0f, 4.0f, 5.0f, 6.0f,
                 4.0f, 4.0f, 5.0f, 6.0f, 7.0f,
                       3.0f, 6.0f, 7.0f, 8.0f,
                             2.0f, 8.0f, 9.0f,
                                   1.0f, 10.0f};

    f64 M1E[] = {0.0f, 1.0f, 2.0f,
                3.0f, 4.0f, 5.0f,
                6.0f, 7.0f, 8.0f};

    matrix M1 = {3, 3, M1E};
    // matrix M2 = {3, 3, I3Array};
    // matrix M3 = {4, 4, I4Array};
    //

    //matrix I3 = CreateIdentity(3);

    //matrix Res = M1*I3;

    //PrintMatrix(&Res);

    // matrix R = M1*M2;
    // PrintMatrix(&R);

    u32 Length = ArrayCount(H5);
    upper_hessenberg H = CreateUpperHessenbergMatrix(H31, ArrayCount(H31));
    //// PrintHessenbergMatrix(&H);

    Eigen(&H);

    //// matrix R1 = H*M3;
    //// PrintMatrix(&R1);

    //upper_hessenberg UH = QRIteration(&H, 100);
    //PrintHessenbergMatrix(&UH);

    //eigen Val = ExtractEigenValuesAndVectors(&UH, &H);
    //PrintEigenValues(&Val);
    //PrintEigenVectors(&Val);

    // PrintHessenbergMatrix(&H);
    //PrintMatrix(&M1);
    //matrix M1Transpose = TransposeMatrix(&M1);
    //PrintMatrix(&M1Transpose);

    // qr QR = QRFactorization(&H);
    // // printf("----------\n");
    // // PrintGivensSteps(QR.GS->Steps, H.Dimension, H.Dimension-1);
    // printf("----------\n");
    // PrintHessenbergMatrix(QR.R);
    // printf("----------\n");
    // PrintMatrix(QR.Q);

    // printf("----------\n");
    // PrintMatrix(QR.GS->Q);



    return 0;
}
#endif
