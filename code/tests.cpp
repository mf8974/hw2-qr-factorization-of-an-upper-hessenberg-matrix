/*********************************************
* File - tests.cpp
* Author - Miha
* Created - 11 Apr 2022
* Description - 
* *******************************************/

#include <stdio.h>
#include "qr_upper_hessenberg.h"

// NOTE(miha): This unit testing library is copied from:
// https://jera.com/techinfo/jtns/jtn002.
#define mu_assert(message, test) do { if (!(test)) return message; } while (0)
#define mu_run_test(test) do { char *message = test(); tests_run++; \
                               if (message) return message; } while (0)
extern int tests_run;
int tests_run = 0;

bool 
ApproxEquals(f64 Value, f64 Other, f64 Epsilon)
{
    return fabs(Value - Other) < Epsilon;
}

static char *
TestIdentityMatrix()
{
    printf("test identity matrix:\n");
    f64 *I33Elements = (f64 *) malloc(sizeof(f64) * 3 * 3);
    matrix I33;
    I33.RowSize = 3;
    I33.ColumnSize = 3;
    I33.Elements = I33Elements;
    CreateIdentity(&I33);
    
    mu_assert("Error row size != 3", I33.RowSize == 3);
    mu_assert("Error column size != 3", I33.ColumnSize == 3);
    mu_assert("Error element at postion 0,0 != 1", I33.Elements[0] == 1.0f);
    mu_assert("Error element at postion 0,1 != 0", I33.Elements[1] == 0.0f);
    mu_assert("Error element at postion 1,1 != 1", I33.Elements[4] == 1.0f);
    mu_assert("Error element at postion 2,2 != 1", I33.Elements[8] == 1.0f);

    return 0;
}

static char *
TestMatrixMultiplication1()
{
    printf("test matrix multiplication 1:\n");
    f64 M33Elements[] = {0.0f, 1.0f, 2.0f,
                         3.0f, 4.0f, 5.0f,
                         6.0f, 7.0f, 8.0f};
    matrix M33 = {3, 3, M33Elements};

    f64 *I33Elements = (f64 *) malloc(sizeof(f64) * 3 * 3);
    matrix I33;
    I33.RowSize = 3;
    I33.ColumnSize = 3;
    I33.Elements = I33Elements;
    CreateIdentity(&I33);

    matrix Result = M33*I33;

    mu_assert("Error element at postion 0,0 != 0", Result.Elements[0] == 0.0f);
    mu_assert("Error element at postion 0,1 != 1", Result.Elements[1] == 1.0f);
    mu_assert("Error element at postion 0,2 != 2", Result.Elements[2] == 2.0f);

    mu_assert("Error element at postion 1,0 != 3", Result.Elements[3] == 3.0f);
    mu_assert("Error element at postion 1,1 != 4", Result.Elements[4] == 4.0f);
    mu_assert("Error element at postion 1,2 != 5", Result.Elements[5] == 5.0f);

    mu_assert("Error element at postion 2,0 != 6", Result.Elements[6] == 6.0f);
    mu_assert("Error element at postion 2,1 != 7", Result.Elements[7] == 7.0f);
    mu_assert("Error element at postion 2,2 != 8", Result.Elements[8] == 8.0f);

    return 0;
}

static char *
TestMatrixMultiplication2()
{
    printf("test matrix multiplication 2:\n");
    f64 M33Elements[] = {0.0f, 1.0f, 2.0f,
                         3.0f, 4.0f, 5.0f,
                         6.0f, 7.0f, 8.0f};
    matrix M33 = {3, 3, M33Elements};
    matrix Result = M33*M33;

    mu_assert("Error element at postion 0,0 != 15", Result.Elements[0] == 15.0f);
    mu_assert("Error element at postion 0,1 != 18", Result.Elements[1] == 18.0f);
    mu_assert("Error element at postion 0,2 != 21", Result.Elements[2] == 21.0f);

    mu_assert("Error element at postion 1,0 != 42", Result.Elements[3] == 42.0f);
    mu_assert("Error element at postion 1,1 != 54", Result.Elements[4] == 54.0f);
    mu_assert("Error element at postion 1,2 != 66", Result.Elements[5] == 66.0f);

    mu_assert("Error element at postion 2,0 != 69", Result.Elements[6] == 69.0f);
    mu_assert("Error element at postion 2,1 != 90", Result.Elements[7] == 90.0f);
    mu_assert("Error element at postion 2,2 != 111", Result.Elements[8] == 111.0f);

    return 0;
}

static char *
TestQRFactorization1()
{
    printf("test QR factorization 1:\n");
    f64 H33Elements[] = { 1.0000f,  -1.8084f,   1.3152f,
                         -6.0828f,  -0.6757f,  -0.0541f,
                                    1.9459f,  -1.3243f};
    upper_hessenberg H33 = CreateUpperHessenbergMatrix(H33Elements, ArrayCount(H33Elements));
    qr QR = QRFactorization(&H33);    
    mu_assert("Error element at postion 0,0", ApproxEquals(QR.R->Elements[0], 6.164451f, 0.000001f));
    mu_assert("Error element at postion 0,1", ApproxEquals(QR.R->Elements[1], 0.373391f, 0.000001f));
    mu_assert("Error element at postion 0,2", ApproxEquals(QR.R->Elements[2], 0.266736f, 0.000001f));

    mu_assert("Error element at postion 1,0", ApproxEquals(QR.R->Elements[3], 0.0f, 0.000001f));
    mu_assert("Error element at postion 1,1", ApproxEquals(QR.R->Elements[4], 2.715509f, 0.000001f));
    mu_assert("Error element at postion 1,2", ApproxEquals(QR.R->Elements[5], -1.848053f, 0.000001f));

    mu_assert("Error element at postion 2,1", ApproxEquals(QR.R->Elements[6], 0.0f, 0.000001f));
    mu_assert("Error element at postion 2,2", ApproxEquals(QR.R->Elements[7], 0.000011f, 0.000001f));

    mu_assert("Error element at postion 0,0", ApproxEquals(QR.Q->Elements[0], 0.162220f, 0.000001f));
    mu_assert("Error element at postion 0,1", ApproxEquals(QR.Q->Elements[1], -0.688258f, 0.000001f));
    mu_assert("Error element at postion 0,2", ApproxEquals(QR.Q->Elements[2], -0.707096f, 0.000001f));

    mu_assert("Error element at postion 1,0", ApproxEquals(QR.Q->Elements[3], -0.986755f, 0.000001f));
    mu_assert("Error element at postion 1,1", ApproxEquals(QR.Q->Elements[4], -0.113148f, 0.000001f));
    mu_assert("Error element at postion 1,2", ApproxEquals(QR.Q->Elements[5], -0.116245f, 0.000001f));

    mu_assert("Error element at postion 2,0", ApproxEquals(QR.Q->Elements[6], 0.0f, 0.000001f));
    mu_assert("Error element at postion 2,1", ApproxEquals(QR.Q->Elements[7], 0.716588f, 0.000001f));
    mu_assert("Error element at postion 2,2", ApproxEquals(QR.Q->Elements[8], -0.697497f, 0.000001f));

    return 0;
}


static char *
TestQRFactorization2()
{
    printf("test QR factorization 2:\n");
    f64 H33Elements[] = { 1.0000f,  -1.8084f,   1.3152f,
                         -6.0828f,  -0.6757f,  -0.0541f,
                                    1.9459f,  -1.3243f};
    upper_hessenberg H33 = CreateUpperHessenbergMatrix(H33Elements, ArrayCount(H33Elements));
    qr QR = QRFactorization(&H33);    
    matrix MatrixR = HessenbergToMatrix(QR.R);
    matrix Result = (*QR.Q)*MatrixR;
    mu_assert("Error element at postion 0,0", ApproxEquals(Result.Elements[0], 1.0f, 0.000001f));
    mu_assert("Error element at postion 0,1", ApproxEquals(Result.Elements[1], -1.8084f, 0.000001f));
    mu_assert("Error element at postion 0,2", ApproxEquals(Result.Elements[2], 1.31520f, 0.000001f));
    mu_assert("Error element at postion 1,0", ApproxEquals(Result.Elements[3], -6.0828f, 0.000001f));
    mu_assert("Error element at postion 1,1", ApproxEquals(Result.Elements[4], -0.6757f, 0.000001f));
    mu_assert("Error element at postion 1,2", ApproxEquals(Result.Elements[5], -0.0541f, 0.000001f));
    mu_assert("Error element at postion 2,1", ApproxEquals(Result.Elements[7], 1.9459f, 0.000001f));
    mu_assert("Error element at postion 2,2", ApproxEquals(Result.Elements[8], -1.3243f, 0.000001f));
    return 0;
}

static char *
TestEigenValues()
{
    printf("test eigenvalues:\n");
    f64 H33Elements[] = { 1.0000f,  -1.8084f,   1.3152f,
                         -6.0828f,  -0.6757f,  -0.0541f,
                                    1.9459f,  -1.3243f};
    upper_hessenberg H33 = CreateUpperHessenbergMatrix(H33Elements, ArrayCount(H33Elements));
    eigen E = Eigen(&H33);
    mu_assert("Error eigen value 0 is different", ApproxEquals(E.Values[0], -3.999998, 0.000001f));
    mu_assert("Error eigen value 1 is different", ApproxEquals(E.Values[1], 3.000014, 0.000001f));
    mu_assert("Error eigen value 2 is different", ApproxEquals(E.Values[2], -0.000016, 0.000001f));

    return 0;
}

static char *
TestEigenVectors()
{
    printf("test eigenvector:\n");
    f64 H33Elements[] = { 1.0000f,  -1.8084f,   1.3152f,
                         -6.0828f,  -0.6757f,  -0.0541f,
                                    1.9459f,  -1.3243f};
    upper_hessenberg H33 = CreateUpperHessenbergMatrix(H33Elements, ArrayCount(H33Elements));
    eigen E = Eigen(&H33);
    mu_assert("Error eigen vector 0,0 is different", ApproxEquals(E.Vectors[0], -0.760366f, 0.000001f));
    mu_assert("Error eigen vector 0,1 is different", ApproxEquals(E.Vectors[1], -1.375044f, 0.000001f));
    mu_assert("Error eigen vector 0,2 is different", ApproxEquals(E.Vectors[2], 1.0f, 0.000001f));

    mu_assert("Error eigen vector 1,0 is different", ApproxEquals(E.Vectors[3], -1.351767, 0.000001f));
    mu_assert("Error eigen vector 1,1 is different", ApproxEquals(E.Vectors[4], 2.222269, 0.000001f));
    mu_assert("Error eigen vector 1,2 is different", ApproxEquals(E.Vectors[5], 1.0f, 0.000001f));
    
    mu_assert("Error eigen vector 2,0 is different", ApproxEquals(E.Vectors[6], -0.084490, 0.000001f));
    mu_assert("Error eigen vector 2,1 is different", ApproxEquals(E.Vectors[7], 0.680551, 0.000001f));
    mu_assert("Error eigen vector 2,2 is different", ApproxEquals(E.Vectors[8], 1.0f, 0.000001f));

    return 0;
}

static char *
RunTests()
{
    mu_run_test(TestIdentityMatrix);
    mu_run_test(TestMatrixMultiplication1);
    mu_run_test(TestQRFactorization1);
    mu_run_test(TestQRFactorization2);
    mu_run_test(TestEigenValues);

    return 0;
}

int
RunTestsAndStatistics()
{
    char *Result = RunTests();

    if(Result != 0)
    {
        printf("%s\n", Result);
    }
    else
    {
        printf("ALL TESTS PASSED\n");
    }
    
    printf("Tests run: %d\n", tests_run);

    return Result != 0;
}

int
main()
{
    RunTestsAndStatistics();

    return 0;
}
