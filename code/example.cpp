// NOTE(miha): In this file we will show you how to work with
// 'qr_upper_hessenberg' library.

// NOTE(miha): We need to include the library - make sure the file
// qr_upper_hessenberg.h is in the same directory as file example.cpp!
#include "qr_upper_hessenberg.h"

int
main()
{

    // NOTE(miha): We can allocate our matrix elements on the stack,
    f64 M33Elements[] = {0.0f, 1.0f, 2.0f,
                         3.0f, 4.0f, 5.0f,
                         6.0f, 7.0f, 8.0f};

    // NOTE(miha): or on the heap. Both ways we get an one-dimensional array of
    // sife 9.
    f64 *I33Elements = (f64 *) malloc(sizeof(f64) * 3 * 3);

    // NOTE(miha): Let's first use some structs from our library.
    // NOTE(miha): Struct 'matrix' needs information about row size, column
    // size, and where the elements are stored.
    matrix M33 = {3, 3, M33Elements};

    // NOTE(miha): We can also init struct in the following way.
    matrix I33;
    I33.RowSize = 3;
    I33.ColumnSize = 3;
    I33.Elements = I33Elements;

    // NOTE(miha): For creating an identity matrix we can use function
    // 'CreateIdentity'. Observe how we need to define row&column size and
    // reserve memory before we can turn matrix into an identy one (with this
    // function). Matrix need to be square and we need to pass it via pointer.
    CreateIdentity(&I33);

    // NOTE(miha): We can multiply two matrices like this.
    matrix Multiplication = M33*I33;

    // NOTE(miha): For printing matrix to the standard output we can use the
    // next function.
    printf("Matrix multiplication:\n");
    PrintMatrix(&Multiplication);

    // NOTE(miha): Multiplication with identity is not interesting, so lets 
    // try multiplying it with itself.
    Multiplication = M33*M33; 
    printf("Matrix multiplication with itself:\n");
    PrintMatrix(&Multiplication);

    // NOTE(miha): Now lets create an upper Hessenberg matrix.
    upper_hessenberg H33;

    // NOTE(miha): Dimension tells us the size of the row, column and main
    // diagonal.
    H33.Dimension = 3;

    // NOTE(miha): Now lets put some elements in the upper Hessenberg matrix.
    f64 H33Elements[] = { 1.0000,  -1.8084,   1.3152,
                         -6.0828,  -0.6757,  -0.0541,
                                    1.9459,  -1.3243};
    // NOTE(miha): Notice how we skipped one element and our elements array
    // 'H33Elements' contains only 8 elements? This is because in upper
    // Hessenberg matrices have only elements in the upper triangle and on the
    // first diagonal bellow the main one. We are only storing elements that
    // can be in the upper Hessenberg matrix.
    
    // NOTE(miha): Link elements with our Hessenberg matrix.
    H33.Elements = H33Elements;

    // NOTE(miha): We also need to put information about the length of the
    // elements array. For this we can use macro 'ArrayCount'.
    H33.Length = ArrayCount(H33Elements);

    // NOTE(miha): Alternativly we can create upper Hessenber matrix in the
    // following way. We just need to pass the elements array and the length of
    // the said array. NOTICE how we are using the same elements array here, so
    // any calculations on the H33 matrix will also change the H33Alternative
    // matrix!
    upper_hessenberg H33Alternative = CreateUpperHessenbergMatrix(H33Elements, ArrayCount(H33Elements));

    // NOTE(miha): Now lets print the upper Hessenber matrix.
    printf("Upper Hessenberg matrix:\n");
    PrintHessenbergMatrix(&H33);

    // NOTE(miha): Our library handles another type of matrices, that is Givens
    // rotation matrices. Givens matrix looks kinda like identity matrix, but 
    // is changed on the I-th and J-th column and row. On those columns and rows
    // it contains a cosinus and sinus of an angle. I and J must be different!
    // NOTE(miha): Lets create a Givens matrix.
    givens G = {2,3, cos(0.5), sin(0.5)};

    // NOTE(miha): Now lets print this matrix. Notice how the print function
    // accepts parameter for dimension, we can have same Givens matrix in many
    // different dimensions!
    printf("Givens matrix:\n");
    PrintGivensMatrix(&G, 5);

    // NOTE(miha): We can also create a Givens matrix of type 'matrix' (what
    // print function does to print the Givens matrix). This function is useful
    // for debugging, not so much for actual computing. As above we need to 
    // pass the number of dimension.
    matrix FromGivenToMatrix = CreateGivensMatrix(G.I, G.J, 5, G.C, G.S);

    // NOTE(miha): While calculating QR Factorization of an upper Hessenberg
    // matrix, Givens rotations are saved in a struct 'givens_steps' which is
    // an array of 'givens' matrices. It also contains information of its
    // length.

    // NOTE(miha): Now lets calculate the QR factorization of upper Hessenberg
    // matrix H33. Result is a struct of type 'qr', which contains upper
    // Hessenberg matrix 'R', orhonormal matrix 'Q', and sequence of Givens
    // matrices 'Steps'.
    qr QR = QRFactorization(&H33);    
    printf("Upper triangular matrix R from QR factorization:\n");
    PrintHessenbergMatrix(QR.R);
    printf("Orthonormal matrix Q from QR factorization:\n");
    PrintMatrix(QR.Q);
    printf("All the Givens rotation steps on the way:\n");
    // NOTE(miha): For printing Givens steps we also need to pass dimension of
    // the mattrix!
    PrintGivensSteps(QR.Steps, QR.R->Dimension);

    // NOTE(miha): To check if the result is correct we can just multiply R and
    // Q and obtain the original H33 matrix. Notice how we need to dereference
    // the QR.Q matrix first.
    matrix MatrixR = HessenbergToMatrix(QR.R);
    matrix IsItTheSame = (*QR.Q) * MatrixR;
    printf("Multiplication between Q and R:\n");
    PrintMatrix(&IsItTheSame);

    // NOTE(miha): After we do not need 'qr' struct anymore, we can free its
    // heap allocated memory the following way.
    FreeQRStruct(&QR);

    // NOTE(miha): Now lets try to find the eigenvalues and eigenvectors of the
    // matrix H33. The library uses algorithm QR iteration for first finding
    // eigenvalues and the solves the linear equation with Gaussian
    // elimination. Library can only work with real numbers.

    // NOTE(miha): Resulting struct 'eigen' contains length of the values and
    // vectors (how many are there), eigenvalues, and eigenvectors. The
    // function Eigen automatically sets the number of iteration to 100, but we
    // can alter that (will be shown bellow).
    eigen E = Eigen(&H33);
    printf("Printing eigenvalues for matrix H33:\n");
    PrintEigenValues(&E);
    printf("Printing eigenvectors for matrix H33:\n");
    PrintEigenVectors(&E);

    // NOTE(miha): Now lets alter the number of iterations from 100 to 20.
    upper_hessenberg QRIterationH = QRIteration(&H33, 20);
    eigen Result = ExtractEigenValuesAndVectors(&QRIterationH, &H33);
    printf("Printing eigenvalues for matrix H33, 20 iterations:\n");
    PrintEigenValues(&E);
    printf("Printing eigenvectors for matrix H33, 20 iterations:\n");
    PrintEigenVectors(&E);

    // NOTE(miha): When we don't need eigen struct anymore we can free its heap
    // allocated memory the following way.
    FreeEigen(&Result);

    return 0;
}
